<?php

global $custom_meta_gallery;

$custom_meta_gallery[] = array(
    "label" => "Gallery image",
    "desc" => "",
    "id" => "_gallery_image",
    "std" => "",
    "type" => "simple_media_picker"
);

$custom_meta_gallery[] = array(
    "label" => "Photos gallery",
    "desc" => "",
    "id" => "_gallery_items",
    "std" => "",
    "type" => "media_picker"
);

// embed gallery
$custom_meta_gallery[] = array(
    "label" => "Embed gallery",
    "desc" => "Click the [ + ] button as many times as how many embeds you want to add, and enter their embed links (youtube, vimeo etc.).",
    "id" => "_gallery_embed_link",
    "std" => "",
    "type" => "list",
    "mod" => "big"
);

// video gallery
$custom_meta_gallery[] = array(
    "label" => "Video gallery",
    "desc" => "Click the [ + ] button as many times as how many videos you want to add, and enter their URL links (youtube, vimeo etc.).",
    "id" => "_gallery_video_link",
    "std" => "",
    "type" => "list"
);