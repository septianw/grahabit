<?php global $jaw_data; ?>
<?php
global $post;
if (jaw_template_get_var('id', '') != '' && get_page(jaw_template_get_var('id', ''))) {
    $query = new WP_Query('page_id=' . jaw_template_get_var('id', ''));
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            ?>
            <div  style="background: url('<?php echo jaw_template_get_var('image_src'); ?>');">
                <?php
                echo the_content();
                ?> 
            </div>
        <?php } ?>
    <?php } ?>
<?php } ?>